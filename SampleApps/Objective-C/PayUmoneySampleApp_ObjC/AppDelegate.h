//
//  AppDelegate.h
//  frameworkIntegrationFromScratch
//
//  Created by Vipin Aggarwal on 16/01/17.
//  Copyright © 2017 PayUmoney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

