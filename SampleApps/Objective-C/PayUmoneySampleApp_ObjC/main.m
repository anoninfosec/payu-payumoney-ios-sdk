//
//  main.m
//  frameworkIntegrationFromScratch
//
//  Created by Vipin Aggarwal on 16/01/17.
//  Copyright © 2017 PayUmoney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
